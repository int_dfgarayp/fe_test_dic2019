package Main;


public class Utils {

	public static String getTaxIDWithoutVD(String AD_Org_ID) {
		String result =  getTaxID(AD_Org_ID);
		if(result==null) {
			result="0";
		}else {
			if(result.contains("-")) {
				result =result.split("-")[0];
			}
		}
		return result;
	}
	
	private static String getTaxID(String AD_Org_ID) {
		String sql = "select taxid from adempiere.ad_orginfo where ad_org_id="+AD_Org_ID+" ;";
		//String result = DB.getSQLValueString(null, sql);
		//TODO
		return "1000001";
	}
	
	public static String getFilledString(String value, int size, boolean isNumber) {
		String res = "";
		
		if(value.length()>size) {
			value = value.substring(0, size);
		}
		res = value;
		for (int i = 0; i < (size - value.length()); i++) {
			if (!isNumber) {
				res = res + ' ';
			} else {
				res = '0' + res;
			}
		}
		return res;
	}
	
	public static String getInvoiceSecuence() {
		String sql = "select to_hex(nextval('dian_doc_sec'));";
		//String result = DB.getSQLValueString(null, sql);
		//TODO
		return "1000007";
	}
}
