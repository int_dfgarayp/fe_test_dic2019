package Main;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.xml.sax.SAXException;

import colombia.dian.wcf.WcfDianCustomerServicesLocator;



public class Main {

	public static Logger logger = Logger.getLogger(Main.class.getName()); 
	public static String SOFTWARE_ID = "1e6c52d3-d906-4bde-aed5-001d0522c279";
	public static String PIN = "SmarFe*";
	
	@FunctionalInterface
	interface test{
		String doit(String ref);
		
		static test test1() {
			return ref -> ref+"51";
		}
	}

	
	public static void main(String[] args) {
		
		
		
		//*** 1. bring invoice XML without sign canonicalizar
	
		String digest_value1 = createDigestValue("GenericaC.xml");
		
	    //*** 4. conon key info //TODO tener encuenta el id del key info sea el mismo que se le asigna al digest value
	    
		String digest_value2 = createDigestValue("keyInfo.xml");
	    
	    //****** fill sign *******
		//Cargar y llenar el xml template para firma //TODO load as DOM
	    String sign_xml =  getXML_as_String("Signature.xml");
	    sign_xml = sign_xml.replace("ref1_digest_value", digest_value1);
	    
	    
	    //TODO crear el 2do digest value
	    sign_xml = sign_xml.replace("ref2_digest_value", digest_value2);
	    

	    //TODO crear el 3do digest value
	    sign_xml = sign_xml.replace("ref3_digest_value", digest_value1);
	    
	    //guarda el archivo de firma diligenciado, solo para verificarlo //TODO eliminar lineas
	    saveStringOnFile(getFullPath()+File.separator+"SignatureFilled.xml", sign_xml);
		
	    //Unificar XMLs
	    String generic_xml_s = getXML_as_String("GenericaC.xml");
	    generic_xml_s =  generic_xml_s.replace("<!--_signature_-->", sign_xml);
	    saveStringOnFile(getFullPath()+File.separator+"Generica_Full.xml", generic_xml_s);
	    
	    logger.log(Level.FINE, "DONE");
		
		int numDeFacturas=1;
		
		//Generate SoftwareSecurityCode > specification # 11.4
		byte[] SoftwareSecurityCode= DigestUtils.sha384((SOFTWARE_ID+PIN+numDeFacturas).getBytes());
		logger.log(Level.SEVERE, "SoftwareSecurityCode : "+Base64.getEncoder().encodeToString(SoftwareSecurityCode));
		
		//result: gJrXxcAip7O+EZcMZcyVgKwJ301u5x2+iV4IrunQSnMpU7RL/OUtljYNx0EhWeJI
		
		//CUFE 11.1
		//TODO insert CUFE algorithm
		
		
	}
	
	public static String createDigestValue(String fileName) {
		File generi_nosigned_xml = new File(getFullPath()+File.separator+fileName);
		byte[] canonXmlBytes=null;
		try {
			canonXmlBytes = createCanonizedDocument(fileName,generi_nosigned_xml, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,e.getMessage());
			return null;
		} 
	    
	    //*** 2. apply sha256
	    byte[] hash =  (DigestUtils.sha256(canonXmlBytes));
	    
	    //*** 3. encode 64 base
	    return Base64.getEncoder().encodeToString(hash);
	}
	
	public static String getFullPath() {
		return "./Resources".replace("\\", File.separator);
	}
	
	public String createFileName1(int AD_org_ID, String trxName, String prefix,String doctype) {
		
		StringBuilder result= new StringBuilder();
		result.append(prefix);
		result.append(doctype);
		result.append(Utils.getFilledString(Utils.getTaxIDWithoutVD( ""+AD_org_ID), 10, true) );
		result.append(Utils.getFilledString(Utils.getInvoiceSecuence(), 10, true) );
		result.append(IDIANFormat.getFileExtension(prefix) );
		
		return result.toString();
	}
	
	/**
	 * to canonize the document
	 * @param File
	 * @return
	 */
	public static byte[] createCanonizedDocument(String fileName, File generic_xml, boolean save) throws CanonicalizationException, ParserConfigurationException, IOException, SAXException, InvalidCanonicalizerException {
		org.apache.xml.security.Init.init(); //start the service to canonize
		Canonicalizer canon = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS);
		byte[] bFile = Files.readAllBytes(generic_xml.toPath());
		byte canonXmlBytes[] = canon.canonicalize(bFile);
		String canonXmlString = new String(canonXmlBytes);
		if(save) {
			saveStringOnFile(getFullPath()+File.separator+fileName+"Canonized.xml", canonXmlString);
		}
		return canonXmlBytes;
	}
	
	public static String getXML_as_String(String fileName) {
		 File xml;
		 String result = null;
		try {
			//TODO set a default file name for sign
			xml = new File(getFullPath()+File.separator+fileName);
			byte[] bytes = Files.readAllBytes(xml.toPath());
			result = new String(bytes,"UTF-8");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = null;
		}
		return result;
	}
	
	public static String getFileAsString(File sign_xml) {
		byte[] bytes;
		try {
			bytes = Files.readAllBytes(sign_xml.toPath());
			return  new String(bytes,"UTF-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static void saveStringOnFile(String filePath,String canonXmlString)  {
		
		try {
			System.out.println(canonXmlString);
			BufferedWriter writer = Files.newBufferedWriter(new File(filePath).toPath());
			writer.write(canonXmlString);
		    writer.close();
		} catch (IOException e) {
			logger.log(Level.WARNING, e.getMessage());
		}
	   
	}
}
