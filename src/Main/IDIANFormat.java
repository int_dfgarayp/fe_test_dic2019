package Main;

import java.io.File;

public interface IDIANFormat {

	public static final String DOC_TYPE_INVOICE		= "f";
	public static final String DOC_TYPE_CREDIT_NOTE	= "c";
	public static final String DOC_TYPE_DEBIT_NOTE	= "d";
	
	public static final String PREFIX_XML="face_";
	public static final String PREFIX_ZIP_ONE="ws_";
	public static final String PREFIX_ZIP_VARIOUS="mft_";
	
	
	public static final String XSD_SOAP= 				"org.smartjsp.einvoicing"+File.pathSeparator+"resources"+File.pathSeparator+"XSD"+File.pathSeparator+"oasis-200401-wss-wssecurity-secext-1.0.xsd";
	public static final String XSD_DIAN_UBL_PATH= 		"org.smartjsp.einvoicing"+File.pathSeparator+"resources"+File.pathSeparator+"XSD"+File.pathSeparator+"r1"+File.pathSeparator+"DIAN_UBL.xsd";
	public static final String XSD_DIAN_UBL_STT_PATH= 	"org.smartjsp.einvoicing"+File.pathSeparator+"resources"+File.pathSeparator+"XSD"+File.pathSeparator+"r1"+File.pathSeparator+"DIAN_UBL_Structures.xsd";
	public static final String XSD_CORE_SCHEMA_PATH= 	"org.smartjsp.einvoicing"+File.pathSeparator+"resources"+File.pathSeparator+"XSD"+File.pathSeparator+"r1"+File.pathSeparator+"xmldsig-core-schema.xsd";
	
	public static final String UBLCommenA= "org.smartjsp.einvoicing"+File.pathSeparator+"resources"+File.pathSeparator+"XSD"+File.pathSeparator+"UBL2"+File.pathSeparator+"common"+File.pathSeparator+"UBL-CommonAggregateComponents-2.0.xsd";
	
	public static String getFileExtension(String prefix) {
		if(prefix.equals(PREFIX_XML))
			return ".xml";
		else if(prefix.equals(PREFIX_ZIP_ONE) || prefix.equals(PREFIX_ZIP_VARIOUS))
			return ".zip";
		else
			return null;
	}
}
