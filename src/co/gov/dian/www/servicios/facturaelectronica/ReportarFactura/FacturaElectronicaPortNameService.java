/**
 * FacturaElectronicaPortNameService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package co.gov.dian.www.servicios.facturaelectronica.ReportarFactura;

public interface FacturaElectronicaPortNameService extends javax.xml.rpc.Service {
    public java.lang.String getfacturaElectronicaPortNameSoap11Address();

    public co.gov.dian.www.servicios.facturaelectronica.ReportarFactura.FacturaElectronicaPortName getfacturaElectronicaPortNameSoap11() throws javax.xml.rpc.ServiceException;

    public co.gov.dian.www.servicios.facturaelectronica.ReportarFactura.FacturaElectronicaPortName getfacturaElectronicaPortNameSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
